let Ctx;
window.addEventListener("load", Init);

let Drawing, Ui;
let Canvas, Buffer, Joystick, Colorwheel; // global

let INITDRAW = { //drawing state
  windows: [{scale: Math.min(window.innerWidth, window.innerHeight)/2,
             center: [window.innerWidth/2, window.innerHeight/2]}],
  styles: [{width: 2, color: "#000"}], // {width, color}
  inputStrokes:[], // [dx1, dy1, dx2, dy2...]
  strokeCount: 0, // use for undo/redo
  drawStrokes: [], // {inputStrokeIndex, start, windowIndex, styleIndex, toolIndex)
  // graveyardStrokes: [], // graveyard for strokes overwritten after undoing.
  precision: 3.0,
}
let INITUI = {
  statusText: "Test status",
  buttons: ["=", "v", "<", ">", "+", "-", "/", "x"],
  displayWindow: {scale: Math.min(window.innerWidth, window.innerHeight)/2, center: [window.innerWidth/2, window/innerHeight/2]},
  displayOffset: [0, 0],
  currentStroke: null,
  cursorPoint: [window.innerWIDTH/2, window.innerHEIGHT/2],
  clickPoint: null,
  penColor: '#000',
  penSize: 2,
  toolIndex: 0,
  styleIndex: 0,
  focus: CANVASFOCUS,
  zoomWindowIndex: 0,
  inboundWindows: {},
  inboundStrokes: {},
}

function Init(){
  Drawing = copy(INITDRAW);
  Canvas = document.getElementById('drawcanvas');
  Buffer = document.createElement('canvas');
  Ctx = Canvas.getContext("2d");
  Ui = copy(INITUI);
  // Ui.toolIndex = 1;
  // experiment with zoomwindow
  let zoomWindow = copy(Ui.displayWindow);
  zoomWindow.zoom = CURSORZOOM;
  zoomWindow.origin = [0,0];//[-Canvas.width/4, -Canvas.height/4];
  Drawing.windows.push(zoomWindow);
  Ui.zoomWindowIndex = Drawing.windows.length - 1;
  //Ui.zoomWindow = copy(Ui.displayWindow);
  //Ui.zoomWindow.zoom = 4;
  Resize();
  Events();
}

function Resize(){
  Canvas.width = window.innerWidth;
  Canvas.height = window.innerHeight;
  Buffer.width = Canvas.width;
  Buffer.height = Canvas.height;
  let w2 = Canvas.width/2;
  let h2 = Canvas.height/2;
  let scale = Math.min(w2, h2);
  // when window is resized, you must save it.
  Ui.displayWindow = copy(Ui.displayWindow);
  Ui.displayWindow.scale = Math.min(w2, h2);
  DrawAll(Canvas, Buffer, Ui, Drawing);
  //fixWindow(Drawing, Ui);
}

function copy(x){
  return x? JSON.parse(JSON.stringify(x)) : x;
}
