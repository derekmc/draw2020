
function Events(){
  let lastEvent = 0;
  function eventWait(){
     let now = Date.now();
     let delay = now - lastEvent;
     if(delay > ANIMATEDT){
        lastEvent = now;
        return false; }
     return true;
  }
  window.addEventListener('resize', Resize);

  window.addEventListener('mousedown', cursorDown);
  window.addEventListener('touchstart', cursorDown);

  window.addEventListener('mousemove', cursorMove);
  window.addEventListener('touchmove', cursorMove);

  window.addEventListener('mouseup', cursorUp);
  window.addEventListener('touchend', cursorUp);
  // TODO use right click or mutli-touch to move window
  function cursorDown(e){
    lastEvent = Date.now();
    //Ctx.beginPath();
    if(e.touches){
      e.clientX = e.touches[0].clientX;
      e.clientY = e.touches[0].clientY;
    }

    let scale = Math.min(Canvas.width, Canvas.height)/2;
    let x = e.clientX;
    let y = e.clientY;
    let button = getButton(Ui.buttons, Canvas, x, y);
    console.log('x, y', x, y);
    if(button >= 0){
      console.log("Button \"" + button + "\" Clicked.");
    }
    else{
      updateFocus(Ui, Canvas, e.clientX, e.clientY);
      if(Ui.focus == CANVASFOCUS){
        startStroke(Drawing, Ui, 2 * scale, x, y);
        DrawAll(Canvas, Buffer, Ui, Drawing);
      }
      else if(Ui.focus == JOYSTICKFOCUS){
        Ui.clickPoint = [x, y];
        let framecount = 0;
        let callback = () => animate(Canvas, Buffer, Ui, Drawing, ANIMATEDT, framecount++);
        if(Ui.animate){
          clearInterval(Ui.animate);
          Ui.animate = null; }
        Ui.animate = setInterval(callback, ANIMATEDT);
      }
      else if(Ui.focus == TOOLFOCUS){
      }
    }
    //Ctx.moveTo(x + Canvas.width/2, y + Canvas.height/2);
  }
  function updateFocus(ui, canvas, clientX, clientY){
    Ui.focus = CANVASFOCUS;
    let x1, y1, dx1, dy1;
    let r = CONTROLSIZE/2;
    let r2 = r*r;
    x1 = canvas.width - MARGIN - r;
    y1 = canvas.height - MARGIN - r;
    dx1 = clientX - x1;
    dy1 = clientY - y1;
    if(dx1*dx1 + dy1*dy1 < r2){
      ui.focus = JOYSTICKFOCUS;
    }
  }
  function cursorMove(e){
    if(eventWait()) return;
    if(e.touches){
      e.clientX = e.touches[0].clientX;
      e.clientY = e.touches[0].clientY;
    }
    let scale = Math.min(Canvas.width, Canvas.height)/2;
    let x = e.clientX;
    let y = e.clientY;

    if(Ui.currentStroke !== null){
      //Ctx.lineTo(x + Canvas.width/2, y + Canvas.height/2);
      updateStroke(Drawing, Ui, 2 * scale, x, y);
      Ui.cursorPoint = [x, y];
      DrawAll(Canvas, Buffer, Ui, Drawing);
    } else {
      updateFocus(Ui, Canvas, e.clientX, e.clientY);
      Ui.cursorPoint = [x, y];
      DrawFromBuffer(Canvas, Buffer, Ui, Drawing);
    }
  }
  function cursorUp(e){
    lastEvent = Date.now();
    Ui.clickPoint = null;
    let scale = Math.min(Canvas.width, Canvas.height)/2;
    let x, y;
    if(e.touches){
      x = Ui.cursorPoint[0];
      y = Ui.cursorPoint[1];
    }
    if(Ui.anmiate){
      clearInterval(Ui.animate);
      Ui.animate = null }
    if(Ui.currentStroke !== null){
      // Ctx.stroke();
      //try{
        endStroke(Drawing, Ui, 2 * scale, x, y);
      //} catch(err){
        //console.log('error', err);
      //}
      Ui.currentStroke = null;
      DrawAll(Canvas, Buffer, Ui, Drawing);
    }
  }
}
function startStroke(drawing, ui, scale, x, y){
  //console.log('start stroke', x, y);
  let stroke = ui.currentStroke = [];
  ui.cursorPoint = [x, y];
  let len = ui
  let tool = TOOLS[ui.toolIndex];
  // console.log('tool', tool);
	// ToolHandlers[tool](stroke, dx, dy);
	drawing.inputStrokes.push(stroke);
	let inputStrokeIndex = drawing.inputStrokes.length - 1;
	let windowIndex = ui.zoomWindowIndex;
	let styleIndex = ui.styleIndex;
	let toolIndex = ui.toolIndex;
	let drawStroke = {
    start: [x, y],
		inputStrokeIndex: inputStrokeIndex,
		windowIndex: windowIndex,
		styleIndex: styleIndex,
		toolIndex: toolIndex,
		bounds: null,
	};
	drawing.drawStrokes.push(drawStroke);
	let index = drawing.drawStrokes.length - 1;
}

function animate(canvas, buffer, ui, drawing, dt, framecount){
  let dx=0, dy=0;
  // console.log('animate');
  if(ui.clickPoint){
    // console.log('clickPoint, cursorPoint', ui.clickPoint, ui.cursorPoint);
    dx = PANSPEED/dt * (ui.clickPoint[0] - ui.cursorPoint[0]); 
    dy = PANSPEED/dt * (ui.clickPoint[1] - ui.cursorPoint[1]); 
  }
  ui.displayOffset[0] += dx;
  ui.displayOffset[1] += dy;
  if(framecount % REDRAWCYCLE == 0){
    DrawAll(canvas, buffer, ui, drawing);
  } else {
    DrawFromBuffer(canvas, buffer, ui, drawing);
  }
}

function updateStroke(drawing, ui, scale, x, y){
  if(ui.currentStroke === null) return;
  let dx = x - ui.cursorPoint[0]
  let dy = y - ui.cursorPoint[1];
  let tool = TOOLS[ui.toolIndex];
  ToolHandlers[tool](ui.currentStroke, dx, dy);
}

function endStroke(drawing, ui, scale, x, y){
  // console.log('end stroke', x, y);
  let dx = x - ui.cursorPoint[0];
  let dy = y - ui.cursorPoint[1];
  //ui.cursorPoint = null;
  let tool = TOOLS[ui.toolIndex];
  let inputStrokeIndex = drawing.inputStrokes.length - 1;
  let stroke = ToolHandlers[tool](ui.currentStroke, dx, dy, true);
  if(stroke == null){
    --drawing.drawStrokes.length; }
  else{
    drawing.inputStrokes[inputStrokeIndex] = stroke; }
  ui.currentStroke = null;
  //}
}

