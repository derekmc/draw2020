
function error(msg){
  // console.trace(msg);
  throw new Error(msg);
}

function DrawAll(canvas, buffer, ui, drawing){
  MainDraw(canvas, buffer, ui, drawing, ui, true);
}
function DrawFromBuffer(canvas, buffer, ui, drawing){
  MainDraw(canvas, buffer, ui, drawing, false);
}

function DrawingLayout(ui, drawing, canvas){
  let w = canvas.width;
  let h = canvas.height;
  ctx.clearRect(0, 0, w, h);
  let x0 = w/2
  let y0 = h/2;
  let size = 100;
  let twobuttonrows = false;
  let vertical = w < h;;
  let fl = Math.floor;
  // Adjust layout for button space.
  // There are 8 buttons.
  // buttons are laid out on the top or the left,
  // along whichever dimension is smaller.
  // If the smaller dimension is less than 240,
  // then there are 2 rows of buttons.
  let btns = ui.buttons.length; // the number of button "squares"
  let btnsize = 0;
  if(vertical){  // vertical layout
     twobuttonrows = w/btns < MINBUTTONSIZE;
     btnsize = w/btns;
     if(twobuttonrows){
        btnsize = w/fl((btns + 1)/2); }
     h -= btnsize * (twobuttonrows? 2 : 1);
     y0 += btnsize/ (twobuttonrows? 1 : 2);
  } else { // horizontal layout
     twobuttonrows = h/btns < MINBUTTONSIZE;
     btnsize = h/btns;
     if(twobuttonrows){
        btnsize = h/fl((btns + 1)/2); }
     w -= btnsize * (twobuttonrows? 2 : 1);
     x0 += btnsize/ (twobuttonrows? 1 : 2);
  }
  let w2 = w/2;
  let h2 = h/2;
  //let cursorWindow = drawing.windows[ui.zoomWindowIndex];
  let zoom = drawing.precision;
  let winx = x0 - w2/zoom;
  let winy = y0 - h2/zoom;
  let winw = w/zoom;
  let winh = h/zoom;
  let x, y;
  ui.displayWindow.center[0] = x0;
  ui.displayWindow.center[1] = y0;
  ui.displayWindow.scale = Math.min(w2, h2);
}

function MainDraw(canvas, buffer, ui, drawing, redraw){
  let ctx = canvas.getContext("2d");
  let w = canvas.width;
  let h = canvas.height;
  ctx.clearRect(0, 0, w, h);
  let x0 = w/2
  let y0 = h/2;
  let size = 100;
  let twobuttonrows = false;
  let vertical = w < h;;
  let fl = Math.floor;
  // Adjust layout for button space.
  // There are 8 buttons.
  // buttons are laid out on the top or the left,
  // along whichever dimension is smaller.
  // If the smaller dimension is less than 240,
  // then there are 2 rows of buttons.
  let btns = ui.buttons.length; // the number of button "squares"
  let btnsize = 0;
  if(vertical){  // vertical layout
     twobuttonrows = w/btns < MINBUTTONSIZE;
     btnsize = w/btns;
     if(twobuttonrows){
        btnsize = w/fl((btns + 1)/2); }
     h -= btnsize * (twobuttonrows? 2 : 1);
     y0 += btnsize/ (twobuttonrows? 1 : 2);
  } else { // horizontal layout
     twobuttonrows = h/btns < MINBUTTONSIZE;
     btnsize = h/btns;
     if(twobuttonrows){
        btnsize = h/fl((btns + 1)/2); }
     w -= btnsize * (twobuttonrows? 2 : 1);
     x0 += btnsize/ (twobuttonrows? 1 : 2);
  }
  let w2 = w/2;
  let h2 = h/2;
  //let cursorWindow = drawing.windows[ui.zoomWindowIndex];
  let zoom = drawing.precision;
  let winx = x0 - (w2-MOVEMARGIN)/zoom;
  let winy = y0 - (h2-MOVEMARGIN)/zoom;
  let winw = (w - 2 * MOVEMARGIN)/zoom;
  let winh = (h - 2 * MOVEMARGIN)/zoom;
  let x, y;

  if(redraw){
     // resetWindow(drawing, ui);
     DrawDrawing(buffer, drawing, ui);
  }
  ctx.drawImage(buffer, ui.displayOffset[0], ui.displayOffset[1]);
  //ui.displayOffset

  if(ui.cursorPoint){
    DrawCursor(canvas, drawing, ui);
  }

  // draw window shadow
  if(drawing.precision > 1){
    ctx.fillStyle = SHADOWCOLOR;
    ctx.fillRect(x0-w2, y0-h2, w, winy-y0 + h2 + OVERLAP);
    ctx.fillRect(x0-w2, winy, winx-x0+w2, winh);
    ctx.fillRect(winx + winw, winy, winx, winh);
    ctx.fillRect(x0-w2, winy + winh - OVERLAP, w, winy + OVERLAP*2);
  }

  // drawmove margin
  let marg = MOVEMARGIN;
  let olap = OVERLAP;
  ctx.fillStyle = SHADOWCOLOR;
  ctx.fillRect(x0-w2, y0-h2, w, marg);
  ctx.fillRect(x0-w2, y0-h2 + marg, marg, h-2*marg);
  ctx.fillRect(x0+w2 - marg, y0-h2 + marg, marg, h-2*marg);
  ctx.fillRect(x0-w2, y0+h2-marg-olap, w, marg + 2*olap);

  // Draw status text.
  if(ui.statusText && ui.statusText.length){
    // ctx.clearRect(x0-w2, y0-h2+STATUSSIZE, w, STATUSSIZE);
    ctx.fillStyle = STATUSBG;
    let size = Math.min(MAXSTATUSRATIO * Math.min(w, h), STATUSSIZE);
    ctx.fillRect(x0-w2, y0-h2+size/3, w, size);
    ctx.fillStyle = "white";
    ctx.textAlign = "center";
    ctx.font = (5/6) * size + "px " + BUTTONFONT;
    ctx.fillText(ui.statusText, x0, y0-h2 + (0.78)*size + size/3);
  } 


  ctx.strokeStyle="rgba(0,0,0,0.5)";
  ctx.lineWidth=1;
  {
    ctx.fillStyle = "black";
    
    if(vertical){
      ctx.fillRect(0, 0, canvas.width, btnsize * (twobuttonrows? 2 : 1)); }
    else{
      ctx.fillRect(0, 0, btnsize * (twobuttonrows? 2 : 1), canvas.height); }
    ctx.fillStyle = "white";
    if(twobuttonrows){
      let rows = fl((btns + 1)/2); // or columns if vertical
      let skip = vertical? rows : 2; // skip the square right next to the first button.
      for(let i=0; i<ui.buttons.length; ++i){
        let button = ui.buttons[i];
        // let a = i < rows ? 0 : btnsize;
        // let b = i%rows * btnsize;
        let c = (i % 2) * btnsize;
        let d = fl(i/2) * btnsize;
        let x = btnsize/6 + (vertical? d : c);
        let y = 5 * btnsize/6 + (vertical? c : d);
        ctx.font = btnsize + "px " + BUTTONFONT;
        ctx.textAlign = "left";
        ctx.fillText(button, x, y);
      } 
    }
    else{
      for(let i=0; i<ui.buttons.length; ++i){
        let button = ui.buttons[i];
        let x = btnsize/6 + (vertical? i * btnsize : 0);
        let y = -btnsize/6 + (vertical? btnsize : (i + 1) * btnsize);
        ctx.font = btnsize + "px " + BUTTONFONT;
        // ctx.font = "100px arial";
        ctx.textAlign = "left";
        
        ctx.fillText(button, x,y);
        // console.log("button", button);
      } 
    }
    /*
    // draw buttons
    m = BUTTONMARGIN;
    let fl = Math.floor;
    for(let i=0; i<BUTTONCOUNT; ++i){
      let a = twobuttonrows? fl(2 * i/btns) : 0;
      x = twobuttonrows? fl(2 * i / BUTTONCOUNT) : ;
      y = h - r - m;
      ctx.fillStyle = 'rgba(0,0,0,0.5)';
      ctx.beginPath();
      ctx.moveTo(x + r, y);
      ctx.arc(x, y, r, 0, 2 * Math.PI);
      ctx.stroke();
      ctx.fill();
      ctx.beginPath();
      ctx.arc(x, y, ui.penSize/2, 0, 2*Math.PI);
      ctx.fillStyle = ui.penColor;
      x = winx + x/zoom;
      y = winy + y/zoom;
      r = r/zoom;
      m = m/zoom;
      ctx.beginPath();
      ctx.fillStyle = SHADOWCOLOR;
      ctx.moveTo(x + r, y);
      ctx.arc(x, y, r, 0, 2 * Math.PI);

      ctx.fill();
      if(ui.focus == "ToolFocus") ctx.fill(), ctx.fill(); // highlight
    }
*/
  }
  //ctx.clearRect(w2*(1 - 1/zoom), h2 * (1 - 1/zoom), w/zoom, h/zoom);
}
function DrawDrawing(canvas, drawing, ui){
  let ctx = canvas.getContext("2d");
  let w = canvas.width;
  let h = canvas.height;
  let w2 = w/2;
  let h2 = h/2;
  let x, y;
  ctx.clearRect(0,0,w,h);
  //ctx.fillRect(0, 0, w, h);
  //ctx.clearRect(winx, winy, winw, winh);
  let drawStrokes = drawing.drawStrokes;
  ctx.lineWidth = ui.penSize;
  ctx.strokeStyle = "black";
  ctx.beginPath();
  let lastStyle = -1;
  let displayWindow = {center: [w2, h2], scale: Math.min(w2, h2)};
  ui.displayWindow = displayWindow;
  for(let i=0; i<drawStrokes.length; ++i){
    // TODO organize by window.
    let drawStroke = drawStrokes[i];
    let inputStroke = drawing.inputStrokes[drawStroke.inputStrokeIndex];
    let style = drawing.styles[drawStroke.styleIndex];
    let inputWindow = drawing.windows[drawStroke.windowIndex];
    console.log("displayWindow: ", displayWindow);
    let input_pt = [drawStroke.start[0], drawStroke.start[1]];
    let display_pt = [0, 0];
    if(!style.color) error("style has no color");
    if(!style.width) error("style has no width");
    ctx.lineWidth = style.width;
    ctx.strokeStyle = style.color;
    

    WindowTransform(display_pt, input_pt, inputWindow, displayWindow);
    for(let j=0; j<inputStroke.length - 1; j += 2){
      if(j == 0) ctx.moveTo(display_pt[0], display_pt[1]);
      else ctx.lineTo(display_pt[0], display_pt[1]);
      let dx = inputStroke[j];
      let dy = inputStroke[j + 1];
      // to fit encoding in 8 bits / 1 byte
      /*while(dx>0 && dx % 255 == 0 && dy>0 && dy%255 == 0 && j<inputStroke.length-2){
        j += 2;
        dx += inputStroke[j];
        dy += inputStroke[j + 1]; }
      if(dx>0 && dx % 255 == 0){
        while(dx>0 && dx % 255 == 0 && j<inputStroke.length-2){
          j += 2;
          dx += inputStroke[j];
          dy += inputStroke[j + 1]; }
      }
      else if(dy>0 && dy % 255 == 0){
        while(dy>0 && dy % 255 == 0 && j<inputStroke.length-2){
          j += 2;
          dx += inputStroke[j];
          dy += inputStroke[j + 1]; }
      }*/
      input_pt[0] += dx;
      input_pt[1] += dy;
      // console.log("Point #" + j/2,  "input_pt", input_pt, "display_pt", display_pt);
      
      WindowTransform(display_pt, input_pt, inputWindow, displayWindow);
    }
    ctx.lineTo(display_pt[0], display_pt[1]);
    if(lastStyle != drawStroke.styleIndex){
      ctx.stroke();
      ctx.beginPath();
      lastStyle = drawStroke.styleIndex;
    }
  }
  ctx.stroke();
}

function DrawCursor(canvas, drawing, ui){
  let ctx = canvas.getContext("2d");
  let w = canvas.width;
  let h = canvas.height;
  let w2 = w/2;
  let h2 = h/2;
  let cursorWindow = drawing.windows[ui.zoomWindowIndex];
  let zoom = cursorWindow.zoom;
  let winx = w2 * (1 - 1/zoom);
  let winy = h2 * (1 - 1/zoom);
  let winw = w/zoom;
  let winh = h/zoom;
  let x, y;

  if(ui.focus == "ToolFocus"){
    document.body.style.cursor = "pointer";
  } else if(ui.focus == "JoystickFocus"){
    document.body.style.cursor = "grab";
  } else if(ui.focus == "CanvasFocus"){
    document.body.style.cursor = "crosshair";
    // draw circle
    if(zoom > 1.001){
      let x = ui.cursorPoint[0] + w2;
      let y = ui.cursorPoint[1] + h2;
      let r = CIRCLESIZE/2;
      let grad = ctx.createRadialGradient(x-r/3.5, y-r/3.5, 0, x, y, r);
      grad.addColorStop(0, 'rgba(0,0,0,0.03)');
      grad.addColorStop(0.9, 'rgba(0,0,0,0.09)');
      grad.addColorStop(1.0, 'rgba(0,0,0,0.12)');
      ctx.fillStyle = grad;
      ctx.beginPath();
      ctx.arc(x, y, r, 0, 2*Math.PI);
      ctx.fill();
      ctx.strokeStyle="rgba(0,0,0,0.5)";
      ctx.lineWidth=1;
      ctx.stroke();
    }

    // draw crosshair
    for(let i=0; i<CROSSHAIRWIDTHS.length; ++i){
      ctx.beginPath();
      let width = CROSSHAIRWIDTHS[i];
      let color = CROSSHAIRCOLORS[i];
      ctx.strokeStyle = color;
      ctx.lineWidth = width;

      // draw cursor
      if(ui.cursorPoint){
        x = ui.cursorPoint[0]/zoom + w2;
        y = ui.cursorPoint[1]/zoom + h2;

        let R = CROSSHAIRSIZE/2;
        let r = CROSSHAIRINNER * R;
        ctx.moveTo(x - R - width/2, y)
        ctx.lineTo(x - r + width/2, y)
        ctx.moveTo(x + R + width/2, y)
        ctx.lineTo(x + r - width/2, y)
        ctx.moveTo(x, y + R + width/2);
        ctx.lineTo(x,  y + r) - width/2;
        ctx.moveTo(x, y - R - width/2);
        ctx.lineTo(x,  y - r + width/2);
      }
      ctx.stroke();
    }
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x+1, y+1);
    ctx.stroke();
  }
}


let drawing, ui, canvas, buffer;
function TestDraw(){
  buffer = document.createElement("canvas");
  canvas = document.getElementById("drawcanvas");
  canvas.width = 500;
  canvas.height = 780;
  let scale = Math.min(canvas.width, canvas.height)/2;
  // TODO windows may optionally have 'bounds', to make copy/paste easier.
  // TODO jsontable (multi-table csv)
  drawing = {
    windows: [
      // TODO rewrite windows
      // {power: 0, precision: 1, center: [-20,-55]}, // power is an exponent that dictates the zoom level.
      {scale: scale, center: [-20,-55]},
      {scale: scale, center: [-20,10]},
    ], // {center, scale}
    styles: [
      {width: 5, color: "#0f8"},
      {width: 3, color: "#f80"},
    ], // {width, color}
    inputStrokes:[
      [80, 0, 30, 10],
      [0, 90, 30, 10],
    ], // [x0, y0, dx1, dy1...]
    // {inputStrokeIndex, windowIndex, start, styleIndex, toolIndex}
    drawStrokes: [
      {inputStrokeIndex: 0, start: [130, 100], windowIndex: 0, styleIndex: 0},
      {inputStrokeIndex: 1, start: [40, 10], windowIndex: 0, styleIndex: 1},
    ],
    clipsets: [ // the strokes which are included in a given clip.
    ],
    clips: [ // used by copy/paste.
    ],
    historyIndex: 0, // used by undo/redo
    precision: 1.0,// 3 or 9
    // zeroth button only shows/hides for 2 rows.
  }
  ui = {
    statusText: "Test status",
    buttons: ["=", "v", "<", ">", "+", "-", "/", "x"],
    displayWindow: {center: [0, 0], scale: 100},
    displayOffset: [0, 0],
    currentStroke: null,
    cursorPoint: [window.innerWIDTH/2, window.innerHEIGHT/2],
    clickPoint: null,
    penColor: '#000',
    penSize: 2,
    toolIndex: 0,
    styleIndex: 0,
    focus: "CanvasFocus",
    zoomWindowIndex: 0,
    inboundWindows: {},
    inboundStrokes: {},
  }
  // console.log("Drawing to canvas");
  // DrawDrawing(canvas, drawing, ui);
  DrawAll(canvas, buffer, ui, drawing);
  DrawCursor(canvas, drawing, ui);

  let windows = [
    {zoom: 1, center: [0,0]},
  ];
}

window.addEventListener("load", TestDraw);
