
var browser = (typeof module == "undefined");

// Transform(point, win1, win2)
// 
// transforms the point, from a win1 reference,
// to a win2 reference.
// 
// window : {
//   center: [x, y],
//   scale: 1.0,
// }
//
// The default window has center [w/2, h/2],
// and scale min(w, h)/2;
function WindowTransform(result, point, win1, win2){
    let center1 = win1.center,
        center2 = win2.center,
        scale1 = win1.scale,
        scale2 = win2.scale;
    let x = point[0], y = point[1],
        x1 = center1[0], y1 = center1[1],
        x2 = center2[0], y2 = center2[1];
    if(result == null) result = [0, 0];
    result[0] = scale2 * (x - x1)/scale1 + x2;
    result[1] = scale2 * (y - y1)/scale1 + y2;
    return result;
}

function TestDrawUtil(){
    let echo = x => console.log(x);

    echo("Testing 'DrawUtil'");
    let eps = 0.0000000001;
    let eq = (a, b) => (Math.abs(a[0] - b[0]) < eps &&
                        Math.abs(a[1] - b[1]) < eps);
    let assert = (val, msg) => {
        if(!val) echo(" Assertion Failed: " + msg)};
    let A = {
        center: [3,4],
        scale: 3, }
    let B = {
        center: [1,2],
        scale: 0.5, }
    let a1 = [6, 7],
        a2 = [0, 1],
        a3 = [6, 1],
        a4 = [0, 7];
    let b1 = [1.5, 2.5],
        b2 = [0.5, 1.5],
        b3 = [1.5, 1.5],
        b4 = [0.5, 2.5];

    let pt = [0,0];
    let AtoB = (x => WindowTransform(pt, x, A, B));
    let BtoA = (x => WindowTransform(pt, x, B, A));

    assert(eq(b1, AtoB(a1)), "b1 == AtoB(a1)")
    assert(eq(a1, BtoA(b1)), "a1 == BtoA(b1)")
    assert(eq(b2, AtoB(a2)), "b2 == AtoB(a2)")
    assert(eq(a2, BtoA(b2)), "a2 == BtoA(b2)")
    assert(eq(b3, AtoB(a3)), "b3 == AtoB(a3)")
    assert(eq(a3, BtoA(b3)), "a3 == BtoA(b3)")
    assert(eq(b4, AtoB(a4)), "b4 == AtoB(a4)")
    assert(eq(a4, BtoA(b4)), "a4 == BtoA(b4)")

    echo("DrawUtil: Tests Finished.");
}

if(!browser || 
    (typeof TESTALL != "undefined" && TESTALL) ||
    (typeof TESTDRAWUTIL != "undefined" && TESTDRAWUTIL)){
        TestDrawUtil();
}

