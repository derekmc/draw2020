
var browser = (typeof module == "undefined");

function echo(...args){
    if(browser){
        let container = document.body;
        let line = null;
        if(args.length > 1){
            container = args[args.length - 1];
        }
        if(args.length > 0){
            line = document.createTextNode(args[0]);
        }
        let br = document.createElement("br");
        container.appendChild(br);
        if(line) container.appendChild(line);
    } else {
        console.log(args.join(", "));
    }
}

echo("util.js loaded.");
echo(browser? "In browser." : "In nodejs console.");
