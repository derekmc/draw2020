
// these update inputStroke in place, using the new dx, dy data.
let ToolHandlers = {};

// draw, fill, erase all use original stroke.
//ToolHandlers[ERASE] =
//ToolHandlers[BLOB] =
ToolHandlers[DRAW] = function(stroke, dx, dy, finish){
  if(!stroke || !Array.isArray(stroke)){
    throw new Error("stroke handlers stroke must be array."); }
  stroke.push(dx, dy);
  if(finish) return stroke.length >= 4? stroke : null;
}

/*
// line uses stroke endpoints
ToolHandlers[LINE] = function(stroke, dx, dy, finish){
  if(!stroke || !Array.isArray(stroke)){
    throw new Error("stroke handlers expect result array."); }
  let len = stroke.length;
  if(len == 0){
    stroke[0] = dx;
    stroke[1] = dy;
    stroke[2] = 0;
    stroke[3] = 0;
  }
  else{
    stroke[2] += dx;
    stroke[3] += dy;
  }
  // TODO fit into byte array
  if(finish) return stroke;
}

// Arc uses endpoints and midpoint.
// TODO redo to match function calling format with dx, dy, finish.
ToolHandlers[ARC] = function(stroke, dx, dy, finish){
  // find three points to define the arc.
  if(stroke.length < 7) return null;
  let result = stroke.slice(0, 7);
  let mid = 2*Math.floor((stroke.length - 1)/4) + 1; // length 7-10 -> 3; 11-14 -> 5
  result[3] = stroke[mid];
  result[4] = stroke[mid + 1];
  result[5] = stroke[stroke.length - 2];
  result[6] = stroke[stroke.length - 1];
  return result; }

*/
