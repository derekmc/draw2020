

const DRAW = "Draw";
const ERASE = "Erase";
// const LINE = "Line";
// const ARC = "Arc";
// const BLOB = "Blob";
// const REMOVE = "Remove";

const MOVEMARGIN = 36;
const STATUSSIZE = 2*MOVEMARGIN;
const STATUSFONT = "Arial";
const MAXSTATUSRATIO = 0.129;// maximum size of status as percentage of screen
const STATUSBG = 'rgba(0,0,0,0.5)';
const MOVESPEED = 120; // maximum pixels per second when all the way into the move margin.
const CANVASFOCUS = "CanvasFocus";
const TOOLFOCUS = "ToolFocus";
const JOYSTICKFOCUS = "JoystickFocus";
const DISPLAYPOWERLIMIT = 8;  // the maximum number of powers of two for something to be displayed.
//const CROSSHAIR = "Crosshair";
//const GRAB = "Grab";
//const MOVE = "Move";
//const POINTER = "Pointer";

const TOOLS = [DRAW, ERASE];
const ANIMATEDT = 1000/15;
const REDRAWCYCLE = 30; // how many frames to render in each 'redraw' cycle, ie when panning
const SHADOWCOLOR = 'rgba(0,0,0,0.05)';
const OVERLAP = 0.1;
const PANSPEED = 10;
const SHADOWOFFSET = 8;
const CIRCLESIZE=55;
const CROSSHAIRSIZE=33;
const CROSSHAIRWIDTHS=[6, 2, 1];
const CROSSHAIRCOLORS=['rgba(255,255,255,0.6)', 'rgba(255,0,0,0.75)', 'rgba(0,0,0,0.75)'];
const CROSSHAIRINNER=0.95;
const CURSORZOOM=Math.sqrt(20);
const MARGIN=20;
const BUTTONMARGIN=5;
const MINBUTTONSIZE=55;
const CONTROLSIZE=70;
const JOYSTICKCOLOR='rgba(255,255,255,0.8)';
const BUTTONFONT="monospace";
//const SMALLDIMENSIZE = 240;
